package abc;

public class Rectangle {
	public double length;
	public double width;
	
	public double rectanglePerimeter(){
		return 2*(length+width);
	}
	
	public double rectangleArea(){
		return length*width;
	}
}
