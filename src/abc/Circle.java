package abc;

public class Circle {
	
	public double radius;
	
	double circlePerimeter(){
		return 2*Math.PI*radius;
	}
	
	double circleArea(){
		return Math.PI*Math.pow(radius,2);
	}
}
