package abc;

public class Triangle {
	public double firstSide;
	public double secondSide;
	public double thirdSide;
	
	public double trianglePerimeter(){
		return firstSide+secondSide+thirdSide;
	}
	
	public double triangleArea(){
		double p = (firstSide+secondSide+thirdSide)/2; //semiperimeter
		double area = Math.sqrt(p*(p-firstSide)*(p-secondSide)*(p-thirdSide));
		return area;
	}
}
