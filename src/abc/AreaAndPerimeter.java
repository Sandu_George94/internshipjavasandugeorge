package abc;

import java.util.Scanner;


public class AreaAndPerimeter {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
					
			System.out.println("Press 1 for circle, 2 for triangle and 3 for rectangle then press Enter.");
			
			Scanner optiune = new Scanner(System.in);
			int opt = optiune.nextInt();
			boolean x=true;
			while(x){
				switch (opt) {
				case 1:
					Circle cerc = new Circle();
					System.out.println("Insert radius:");
					Scanner b = new Scanner(System.in);
					String raza = b.nextLine();
					Double c = Double.parseDouble(raza);
					cerc.radius = c;
					System.out.println("Perimetrul cercului este " + cerc.circlePerimeter() + 
							" iar aria este " + cerc.circleArea());
					break;
				
				case 2:
					Triangle triunghi = new Triangle();
					System.out.println("Insert first side:");
					Scanner firstSide = new Scanner(System.in);
					String firstSideString = firstSide.nextLine();
					Double firstSideDouble = Double.parseDouble(firstSideString);
					
					System.out.println("Insert second side:");
					Scanner secondSide = new Scanner(System.in);
					String secondSideString = secondSide.nextLine();
					Double secondSideDouble = Double.parseDouble(secondSideString);
					
					System.out.println("Insert third side:");
					Scanner thirdSide = new Scanner(System.in);
					String thirdSideString = thirdSide.nextLine();
					Double thirdSideDouble = Double.parseDouble(thirdSideString);
					
					if(firstSideDouble>(secondSideDouble+thirdSideDouble) || 
							secondSideDouble>(firstSideDouble+thirdSideDouble) ||
							thirdSideDouble>(firstSideDouble+secondSideDouble)){
						System.out.println("Laturile pe care le-ati introdus nu formeaza un triunghi!");
					}else{
						triunghi.firstSide = firstSideDouble;
						triunghi.secondSide = secondSideDouble;
						triunghi.thirdSide = thirdSideDouble;
						
						System.out.println("Perimetrul triunghiului este " + triunghi.trianglePerimeter() + 
								" iar aria este " + triunghi.triangleArea());
					}
					
					//firstSide.close();
					//secondSide.close();
					//thirdSide.close();
					break;
					
					
				case 3:
					Rectangle dreptunghi  = new Rectangle();
					System.out.println("Insert width:");
					Scanner width = new Scanner(System.in);
					String widthString = width.nextLine();
					Double widthDouble = Double.parseDouble(widthString);
					
					System.out.println("Insert length:");
					Scanner length = new Scanner(System.in);
					String lengthString = length.nextLine();
					Double lengthDouble = Double.parseDouble(lengthString);
					
					dreptunghi.length=lengthDouble;
					dreptunghi.width=widthDouble;
					
					System.out.println("Perimetrul dreptunghiului este " + dreptunghi.rectanglePerimeter() + 
							" iar aria este " + dreptunghi.rectangleArea());
					//width.close();
					//length.close();
					break; 
					
				case 4:
					System.out.println("Have a nice day! :D");
					x=false;
					break;
					
				default:
					System.out.println("Press 1, 2 or 3.");
					break;
				}
				
				if(x){
					
					System.out.println("\nPress 1 for circle, 2 for triangle and 3 for rectangle then press Enter.");
					opt = optiune.nextInt();
				}
			}
			//optiune.close();
	}
}
